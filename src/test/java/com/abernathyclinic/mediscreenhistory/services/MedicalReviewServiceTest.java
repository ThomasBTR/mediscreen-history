package com.abernathyclinic.mediscreenhistory.services;

import com.abernathyclinic.mediscreenhistory.mappers.IMedicalReviewToDtoMapper;
import com.abernathyclinic.mediscreenhistory.models.MedicalReview;
import com.abernathyclinic.mediscreenhistory.models.MedicalReviewDTO;
import com.abernathyclinic.mediscreenhistory.repositories.IMedicalReviewRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(properties = "spring.cloud.config.enabled=false")
@ExtendWith(MockitoExtension.class)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class MedicalReviewServiceTest {

    @Autowired
    MedicalReviewService medicalReviewService;

    @MockBean
    IMedicalReviewRepository medicalReviewRepository;

    private MedicalReview medicalReview;

    @BeforeEach
    void setUp() {
        medicalReview = new MedicalReview();
        medicalReview.setDate(Instant.parse("2022-10-27T10:10:10Z"));
        medicalReview.setNotes("This is the first note.");
        medicalReview.setPatientId(1);
        medicalReview.setNoteId("63591641e3d1a35797e29f04");
    }

    @Test
    void getMedicalReviewsByPatientId_shouldReturnMedicalReview() {
        //Given
        List<MedicalReview> medicalReviews = new ArrayList<>();
        medicalReviews.add(medicalReview);
        MedicalReviewDTO expectedDto = IMedicalReviewToDtoMapper.INSTANCE.medicalReviewToDto(medicalReview);
        when(medicalReviewRepository.findByPatientIdOrderByDateDesc(1)).thenReturn(medicalReviews);
        //Given
        List<MedicalReviewDTO> actualMedicalReviewDtos = medicalReviewService.getMedicalReviewsByPatientId(1);
        //Then
        assertThat(actualMedicalReviewDtos).hasSize(1).contains(expectedDto);
    }

    @Test
    void addMedicalReview() {
        //Given
        when(medicalReviewRepository.insert(medicalReview)).thenReturn(medicalReview);
        MedicalReviewDTO expectedDto = IMedicalReviewToDtoMapper.INSTANCE.medicalReviewToDto(medicalReview);
        //When
        MedicalReviewDTO actualDto = medicalReviewService.addMedicalReview(expectedDto);
        //Then
        assertThat(actualDto).isEqualTo(expectedDto);
    }

    @Test
    void updateMedicalReview() {
        //Given
        when(medicalReviewRepository.findById(medicalReview.getNoteId())).thenReturn(Optional.ofNullable(medicalReview));
        MedicalReview updatedReview = medicalReview;
        updatedReview.setNotes("This is an updated note.");
        updatedReview.setDate(Instant.parse("2022-10-28T15:20:42Z"));
        MedicalReviewDTO expectedDto = IMedicalReviewToDtoMapper.INSTANCE.medicalReviewToDto(updatedReview);
        when(medicalReviewRepository.save(any(MedicalReview.class))).thenReturn(updatedReview);
        //When
        MedicalReviewDTO actualDto = medicalReviewService.updateMedicalReview(expectedDto);
        //Then
        assertThat(actualDto.getNotes()).isEqualTo(expectedDto.getNotes());
        assertThat(actualDto.getNoteId()).isEqualTo(expectedDto.getNoteId());
        assertThat(actualDto.getPatientId()).isEqualTo(expectedDto.getPatientId());
    }
}