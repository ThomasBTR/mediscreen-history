package com.abernathyclinic.mediscreenhistory.repositories;

import com.abernathyclinic.mediscreenhistory.models.MedicalReview;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

@TestPropertySource(locations = "/bootstrap-test.properties")
@DataMongoTest()
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
class IMedicalReviewRepositoryTest {

    @Autowired
    private IMedicalReviewRepository repositoryUnderTest;

    private MedicalReview medicalReview;

    @BeforeEach
    void prepare() {
        medicalReview = new MedicalReview();
        medicalReview.setDate(Instant.now());
        medicalReview.setNotes("This is a note");
        medicalReview.setPatientId(1);
    }

    @Test
    void givenMedicalReview_whenInsert_RetrieveMedicalReviewInserted() {
        //When
        MedicalReview reviewReturned = repositoryUnderTest.insert(medicalReview);
        //Then
        assertThat(reviewReturned).isEqualTo(medicalReview);
    }

    @Test
    void givenMedicalReviewAlreadyInDatabase_whenUpdate_RetrieveUpdatedMedicalReview() {
        //Given
        MedicalReview insertedMedicalReview = repositoryUnderTest.insert(medicalReview);
        MedicalReview updatedMedicalReview = medicalReview;
        updatedMedicalReview.setNotes("This is an updated note.");
        updatedMedicalReview.setDate(Instant.now());
        //When
        MedicalReview retrieveMedicalReview = repositoryUnderTest.save(updatedMedicalReview);
        assertThat(retrieveMedicalReview).isEqualTo(updatedMedicalReview);
        assertThat(retrieveMedicalReview.getNoteId()).isEqualTo(updatedMedicalReview.getNoteId());
        assertThat(retrieveMedicalReview.getPatientId()).isEqualTo(insertedMedicalReview.getPatientId());
        assertThat(retrieveMedicalReview.getNoteId()).isEqualTo(insertedMedicalReview.getNoteId());
    }

}
