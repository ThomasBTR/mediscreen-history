package com.abernathyclinic.mediscreenhistory.utils;

import com.abernathyclinic.mediscreenhistory.configurations.jacksons.JacksonConfiguration;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class UTHelper {

    private static final JacksonConfiguration jacksonConfiguration = new JacksonConfiguration();

    public UTHelper() {
    }

    public static String readFileAsString(String fileRelativePath) throws IOException {
        String basePath = "src/test/resources";
        File file = new File(basePath + "/" + fileRelativePath);
        return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
    }

    public static <T> T stringToObject(String objectString, Class<T> objectClass) throws JsonProcessingException {
        ObjectMapper mapper = jacksonConfiguration.objectMapper();
        return mapper.readValue(objectString, objectClass);
    }

    public static <T> T stringToListObject(String objectString, TypeReference<T> typeReference) throws JsonProcessingException {
        ObjectMapper mapper = jacksonConfiguration.objectMapper();
        return mapper.readValue(objectString, typeReference);
    }
}
