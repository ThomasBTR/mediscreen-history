package com.abernathyclinic.mediscreenhistory.controllers;


import com.abernathyclinic.mediscreenhistory.mappers.IMedicalReviewToDtoMapper;
import com.abernathyclinic.mediscreenhistory.models.MedicalReview;
import com.abernathyclinic.mediscreenhistory.models.MedicalReviewDTO;
import com.abernathyclinic.mediscreenhistory.repositories.IMedicalReviewRepository;
import com.abernathyclinic.mediscreenhistory.configurations.jacksons.JacksonConfiguration;
import com.abernathyclinic.mediscreenhistory.utils.UTHelper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = "spring.cloud.config.enabled=false")
@AutoConfigureMockMvc
@ActiveProfiles("test")
@AutoConfigureDataMongo
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class MedicalReviewControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private IMedicalReviewRepository medicalReviewRepository;

    private static final JacksonConfiguration jacksonConfiguration = new JacksonConfiguration();


    @BeforeEach
    void prepare() throws IOException {
        medicalReviewRepository.deleteAll();
        List<MedicalReview> medicalReviews = UTHelper.stringToListObject(UTHelper.readFileAsString("database/data.json"), new TypeReference<>() {
        });
        medicalReviewRepository.insert(medicalReviews);
    }

    @Test
    void getMedicalReviewsByPatientId() throws Exception {
        //Given
        mockMvc.perform(get("/api/v1/medicalReviews/{patientId}", 2)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(UTHelper.readFileAsString("/expected/patient2.json")));
    }

    @Test
    void addMedicalReviewByPatientId() throws Exception {
        String patient3add = UTHelper.readFileAsString("/request/patient3add.json");
        mockMvc.perform(post("/api/v1/medicalReviews/{patientId}", 3)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(patient3add))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(UTHelper.readFileAsString("/expected/patient3add.json")));
    }

    @Test
    void updateMedicalReviewByPatientId() throws Exception {
        List<MedicalReview> medicalReviews = medicalReviewRepository.findByPatientIdOrderByDateDesc(4);
        MedicalReview updatedMedicalReview = medicalReviews.get(0);
        updatedMedicalReview.setDate(Instant.now());
        updatedMedicalReview.setNotes("Patient states that they are experiencing back pain when seated for a long time. \n No more vertigo felt due to this pain when standing up.");
        MedicalReviewDTO updatedDto = IMedicalReviewToDtoMapper.INSTANCE.medicalReviewToDto(updatedMedicalReview);
        MvcResult result =  mockMvc.perform(put("/api/v1/medicalReviews/{patientId}", 4)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(updatedDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();
        MedicalReviewDTO actualResultDto = UTHelper.stringToObject(result.getRequest().getContentAsString(), MedicalReviewDTO.class);
        assertThat(actualResultDto.getPatientId()).isEqualTo(updatedDto.getPatientId());
        assertThat(actualResultDto.getNoteId()).isEqualTo(updatedDto.getNoteId());
        assertThat(actualResultDto.getNotes()).isEqualTo(updatedDto.getNotes());
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = jacksonConfiguration.objectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}