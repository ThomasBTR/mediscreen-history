package com.abernathyclinic.mediscreenhistory.repositories;

import com.abernathyclinic.mediscreenhistory.models.MedicalReview;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IMedicalReviewRepository extends MongoRepository<MedicalReview, String> {

    public List<MedicalReview> findByPatientIdOrderByDateDesc(int patientId);

    MedicalReview findByNoteId(String noteId);
}
