package com.abernathyclinic.mediscreenhistory.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Data
@Document(collection = "medicalReview")
public class MedicalReview {

    @Id
    private String noteId;
    @Indexed
    private int patientId;
    private String notes;
    private Instant date;

}
