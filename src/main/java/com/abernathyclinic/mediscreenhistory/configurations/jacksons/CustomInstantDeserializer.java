package com.abernathyclinic.mediscreenhistory.configurations.jacksons;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;


/**
 * The type Custom offset date time deserializer.
 */
@Component
public class CustomInstantDeserializer extends JsonDeserializer<Instant> {

    /**
     * Deserialize offset date time.
     *
     * @param jsonParser             the json parser
     * @param deserializationContext the deserialization context
     * @return the offset date time
     * @throws IOException the io exception
     */
    @Override
    public Instant deserialize(final JsonParser jsonParser, final DeserializationContext deserializationContext) throws IOException {
        return Instant.parse(jsonParser.getValueAsString());
    }
}
