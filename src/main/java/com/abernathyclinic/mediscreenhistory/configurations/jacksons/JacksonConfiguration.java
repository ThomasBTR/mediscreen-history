package com.abernathyclinic.mediscreenhistory.configurations.jacksons;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.time.format.DateTimeFormatter;

/**
 * The type Jackson configuration.
 */
@Configuration
public class JacksonConfiguration {


    /**
     * Date time formatter date time formatter.
     *
     * @return the date time formatter
     */
    @Bean
    public DateTimeFormatter dateTimeFormatter() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");
    }

    /**
     * Object mapper object mapper.
     *
     * @return the object mapper
     */
    @Bean
    @Primary
    public ObjectMapper objectMapper() {
        JacksonObjectMapper jacksonObjectMapper = new JacksonObjectMapper();
        return jacksonObjectMapper.getObjectMapper();
    }
}
