package com.abernathyclinic.mediscreenhistory.configurations.jacksons;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;

/**
 * The type Custom offset date time serializer.
 */
@Component
public class CustomInstantSerializer extends JsonSerializer<Instant> {

    /**
     * The Date time formatter.
     */
    private final DateTimeFormatter dateTimeFormatter;

    /**
     * Instantiates a new Custom offset date time serializer.
     *
     * @param dateTimeFormatter the date time formatter
     */
    public CustomInstantSerializer(final DateTimeFormatter dateTimeFormatter) {
        this.dateTimeFormatter = dateTimeFormatter;
    }

    /**
     * Serialize.
     *
     * @param value              the value
     * @param jsonGenerator      the json generator
     * @param serializerProvider the serializer provider
     * @throws IOException the io exception
     */
    @Override
    public void serialize(final Instant value, final JsonGenerator jsonGenerator, final SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(dateTimeFormatter.format(value));
    }
}
