package com.abernathyclinic.mediscreenhistory.services;

import com.abernathyclinic.mediscreenhistory.mappers.IMedicalReviewToDtoMapper;
import com.abernathyclinic.mediscreenhistory.models.MedicalReview;
import com.abernathyclinic.mediscreenhistory.models.MedicalReviewDTO;
import com.abernathyclinic.mediscreenhistory.repositories.IMedicalReviewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MedicalReviewService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicalReviewService.class);

    private final IMedicalReviewRepository medicalReviewRepository;

    public MedicalReviewService(IMedicalReviewRepository medicalReviewRepository) {
        this.medicalReviewRepository = medicalReviewRepository;
    }

    @Transactional
    public List<MedicalReviewDTO> getMedicalReviewsByPatientId(int patientId) {
        List<MedicalReview> medicalReviewsGathered = medicalReviewRepository.findByPatientIdOrderByDateDesc(patientId);
        List<MedicalReviewDTO> medicalReviewDtoMapped = new ArrayList<>();
        medicalReviewsGathered.forEach(medicalReview -> medicalReviewDtoMapped.add(IMedicalReviewToDtoMapper.INSTANCE.medicalReviewToDto(medicalReview)));
        return medicalReviewDtoMapped;
    }

    @Transactional
    public MedicalReviewDTO addMedicalReview(MedicalReviewDTO medicalReviewDTO) {
        MedicalReview medicalReview = IMedicalReviewToDtoMapper.INSTANCE.medicalReviewDtoToMedicalReview(medicalReviewDTO);
        medicalReview = medicalReviewRepository.insert(medicalReview);
        return IMedicalReviewToDtoMapper.INSTANCE.medicalReviewToDto(medicalReview);
    }

    @Transactional
    public MedicalReviewDTO updateMedicalReview(MedicalReviewDTO medicalReviewDTO) {
        Optional<MedicalReview> optionalMedicalReview = medicalReviewRepository.findById(medicalReviewDTO.getNoteId());
        MedicalReview medicalReviewToUpdate = null;
        if (optionalMedicalReview.isPresent()) {
            medicalReviewToUpdate = optionalMedicalReview.get();
            medicalReviewToUpdate.setNotes(medicalReviewDTO.getNotes());
            medicalReviewToUpdate.setDate(Instant.now());
            medicalReviewToUpdate = medicalReviewRepository.save(medicalReviewToUpdate);
        }
        return IMedicalReviewToDtoMapper.INSTANCE.medicalReviewToDto(medicalReviewToUpdate);
    }

    @Transactional
    public MedicalReviewDTO getMedicalReviewByNoteId(String noteId) {
        MedicalReview medicalReview = medicalReviewRepository.findByNoteId(noteId);
        return IMedicalReviewToDtoMapper.INSTANCE.medicalReviewToDto(medicalReview);
    }
}
