package com.abernathyclinic.mediscreenhistory.controllers;

import com.abernathyclinic.mediscreenhistory.api.MedicalReviewsApi;
import com.abernathyclinic.mediscreenhistory.models.MedicalReviewDTO;
import com.abernathyclinic.mediscreenhistory.exceptions.MediscreenHistoryRuntimeException;
import com.abernathyclinic.mediscreenhistory.services.MedicalReviewService;
import io.swagger.v3.oas.annotations.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class MedicalReviewController implements MedicalReviewsApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicalReviewController.class);

    private final MedicalReviewService medicalReviewService;

    public MedicalReviewController(MedicalReviewService medicalReviewService) {
        this.medicalReviewService = medicalReviewService;
    }

    @Override
    public ResponseEntity<List<MedicalReviewDTO>> getMedicalReviewsByPatientId(
            @Parameter(name = "patientId", description = "ID of patient to return", required = true) @PathVariable("patientId") Integer patientId
    ) {
        ResponseEntity<List<MedicalReviewDTO>> medicalReviewsGatheredResponseEntity = null;
        try {
            medicalReviewsGatheredResponseEntity = ResponseEntity.ok(medicalReviewService.getMedicalReviewsByPatientId(patientId));
            LOGGER.info("Medical Reviews gathered for patient with id # {}", patientId);
        } catch (MediscreenHistoryRuntimeException exception) {
            LOGGER.error("Error while gathering medical reviews for patient id # {}", patientId, exception);
        }
        return  medicalReviewsGatheredResponseEntity;
    }

    @Override
    public ResponseEntity<MedicalReviewDTO> getMedicalReviewByNoteId(
            @Parameter(name = "patientId", description = "ID of the patient", required = true) @PathVariable("patientId") Integer patientId,
            @Parameter(name = "noteId", description = "ID of the medical review to return", required = true) @PathVariable("noteId") String noteId
    ) {
        ResponseEntity<MedicalReviewDTO> medicalReviewGathered = null;
        try {
            medicalReviewGathered = ResponseEntity.ok(medicalReviewService.getMedicalReviewByNoteId(noteId));
            LOGGER.info("Medical Review id # {} gathered for patient with id # {}", noteId, patientId);
        } catch (MediscreenHistoryRuntimeException exception) {
            LOGGER.error("Error while gathering medical review id # {} for patient id # {}",noteId, patientId, exception);
        }
        return  medicalReviewGathered;
    }

    @Override
    public ResponseEntity<MedicalReviewDTO> addMedicalReviewByPatientId(
            @Parameter(name = "patientId", description = "ID of patient to return", required = true) @PathVariable("patientId") Integer patientId,
            @Parameter(name = "MedicalReviewDTO", description = "insert a new medicalReview in database", required = true) @Valid @RequestBody MedicalReviewDTO medicalReviewDTO
    ) {
        ResponseEntity<MedicalReviewDTO> addMedicalReview = null;
        try {
            addMedicalReview = ResponseEntity.ok(medicalReviewService.addMedicalReview(medicalReviewDTO));
            LOGGER.info("Added medical review for patient with id # {}", patientId);
        } catch (MediscreenHistoryRuntimeException exception) {
            LOGGER.error("Error while adding medical review for patient id # {}", patientId, exception);
        }
        return addMedicalReview;
    }

    @Override
    public ResponseEntity<MedicalReviewDTO> updateMedicalReviewByPatientId(
            @Parameter(name = "patientId", description = "ID of patient to return", required = true) @PathVariable("patientId") Integer patientId,
            @Parameter(name = "MedicalReviewDTO", description = "update an existing patient in database", required = true) @Valid @RequestBody MedicalReviewDTO medicalReviewDTO
    ) {
        ResponseEntity<MedicalReviewDTO> medicalReviewDtoToUpdate = null;
        try {
            medicalReviewDtoToUpdate = ResponseEntity.ok(medicalReviewService.updateMedicalReview(medicalReviewDTO));
            LOGGER.info("Update medical review with patient id # {}", patientId);
        } catch (MediscreenHistoryRuntimeException exception) {
            LOGGER.error("Error while update medical review for patient id # {}", patientId, exception);
        }
        return medicalReviewDtoToUpdate;
    }
}
