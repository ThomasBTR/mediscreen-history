package com.abernathyclinic.mediscreenhistory.mappers;

import com.abernathyclinic.mediscreenhistory.models.MedicalReviewDTO;
import com.abernathyclinic.mediscreenhistory.models.MedicalReview;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface IMedicalReviewToDtoMapper {

    IMedicalReviewToDtoMapper INSTANCE = Mappers.getMapper(IMedicalReviewToDtoMapper.class);



    MedicalReview medicalReviewDtoToMedicalReview(MedicalReviewDTO medicalReviewDTO);


    MedicalReviewDTO medicalReviewToDto(MedicalReview medicalReview);

}
