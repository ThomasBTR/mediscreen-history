package com.abernathyclinic.mediscreenhistory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class MediscreenHistoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediscreenHistoryApplication.class, args);
	}

}
