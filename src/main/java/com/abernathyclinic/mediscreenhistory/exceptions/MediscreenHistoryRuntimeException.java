package com.abernathyclinic.mediscreenhistory.exceptions;

public class MediscreenHistoryRuntimeException extends RuntimeException{

    public MediscreenHistoryRuntimeException(String s) {
        super(s);
    }

    public MediscreenHistoryRuntimeException(final String s, final Throwable throwable) {
        super(s, throwable);
    }

}
