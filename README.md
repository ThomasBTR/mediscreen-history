<img src="https://img.shields.io/badge/java-%23ED8B00.svg?&style=for-the-badge&logo=java&logoColor=white"/> * * *  <img src="https://img.shields.io/badge/spring%20-%236DB33F.svg?&style=for-the-badge&logo=spring&logoColor=white"/> 



# Mediscreen-History

## Technical

1. Framework: Spring Boot v2.7.5
2. Java 17
3. Database : MongoDB for usage, embedded MongoDB for testing
4. docker build with jib
5. Current version : 2.0.0

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing
purposes.

### Prerequisites

What things you need to install the software and how to install them

- Java 17
- Maven 4.0.0

### Running App

You can run the application in two different ways:

1/ Run it from docker-compose available on [mediscreen-module](https://gitlab.com/mediscreen-apps/mediscreen-modules)
Don't forget to specify witch image (corresponding to the version) you want to run

On the two following method, you will need to run the [config-server](https://gitlab.com/mediscreen-apps/config-server) first.

2/ import the code into an IDE of your choice and run the Application.java to launch the application.

3/ Or import the code, unzip it, open the console, go to the root folder that contains the gradlew file, then execute
the below command to launch the application.


```bash
mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dspring.cloud.config.uri=http://localhost:9101"
```

Running the app from docker : On the source folder of this application, you can build the image docker to your local Docker damon using:
``mvn compile jib:dockerBuild``

Then run the image using :
``docker run -p 9002:9002 mediscreen-history:1.0.0``

### Testing

The app has unit tests written.

To run the tests from maven, go to the folder that contains the pom.xml file and execute the below command.

```bash
mvn clean verify
```

### Technical and Functional Specifications

The specifications were showed in the demo. Please contact the current team in charge of this application to access
these infos.

### Coverage

```bash
mvn clean verify
```
The tests coverage can be accessed after building the app thanks to Jacoco in the target/site/index.html or jacoco.xml
